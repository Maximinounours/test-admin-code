from setuptools import setup

setup (
    name= 'PackageTestCalculateur',
    version='0.0.1',
    author="Maxime et Sophie",
    package=['calculator'],
    description= "TestCalculateur package pour realise des calculs de base en Python",
    license='GNU GPLv3',
    python_requires='>=2.4',
)