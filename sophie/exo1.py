#!usr/bin/env python
#coding : utf-8




class SimpleCalculator:
    def sum(self,a,b):
        return a+b;

    def substract(self,a,b):
        return a-b;

    def multiply(self,a,b):
        return a*b;

    def division (self,a,b):
        return float(a/b);

#Test
a = 5; b = 7;
s = SimpleCalculator();

print(a, '+', b, '=', s.sum(a,b));
print(a, '-', b, '=', s.substract(a,b));
print(a, '*', b, '=', s.multiply(a,b));
print(a, '/', b, '=', s.division(a,b));