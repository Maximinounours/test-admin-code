#!usr/bin/env python
#coding : utf-8

"""Package d'une classe pour une calculatrice"""


class SimpleCalculator(object):
    "calculatrice"
    def __init__(self):
        "Constructeur inutile"

    @staticmethod
    def sum(var_a, var_b):
        "addition de a et b"
        if isinstance(var_a, int) & isinstance(var_b, int):
            return var_a+var_b
        return "ERROR"

    @staticmethod
    def substract(var_a, var_b):
        "soustraction de b a b"
        if isinstance(var_a, int) & isinstance(var_b, int):
            return var_a-var_b
        return "ERROR"

    @staticmethod
    def multiply(var_a, var_b):
        "multiplication de a et b"
        if isinstance(var_a, int) & isinstance(var_b, int):
            return var_a*var_b
        return "ERROR"

    @staticmethod
    def division(var_a, var_b):
        "division de a par b"
        if var_b == 0:
            raise ZeroDivisionError("Division par zero impossible")
        elif isinstance(var_a, int) & isinstance(var_b, int):
            return float(var_a/var_b)
        return "ERROR"
