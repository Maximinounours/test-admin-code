"Classe de test"

import unittest
import logging

import module_calculator

class TestSum(unittest.TestCase):
    "Classe de test"

    logging.warning("Debut test methode sum")

    def setUp(self):
        "init"
        
        self.calculator = module_calculator.SimpleCalculator()

    def test_sum_egalite(self):
        "test egalite resultat"
        resultat = self.calculator.sum(6, 5)
        self.assertEqual(resultat, 11)
        self.assertNotEqual(resultat, 10)
        resultat = self.calculator.sum(-6, -5)
        self.assertEqual(resultat, -11)
        self.assertNotEqual(resultat, 11)
        logging.info("test_sum_egalite reussis")

    def test_sum_entier(self):
        "test des arguments"
        self.assertEqual(self.calculator.sum(6, "5"), "ERROR")
        self.assertNotEqual(self.calculator.sum("6", 5), 11)
        logging.info("test_sum_entier reussis")


class TestSubstract(unittest.TestCase):
    "Classe de test"

    logging.warning("Debut test methode substract")

    def setUp(self):
        "init"
        
        self.calculator = module_calculator.SimpleCalculator()

    def test_substract_egalite(self):
        "test egalite resultat"
        resultat = self.calculator.substract(6, 5)
        self.assertEqual(resultat, 1)
        self.assertNotEqual(resultat, -1)
        resultat = self.calculator.substract(-6, -5)
        self.assertEqual(resultat, -1)
        self.assertNotEqual(resultat, -11)
        resultat = self.calculator.substract(5, 6)
        self.assertEqual(resultat, -1)
        self.assertNotEqual(resultat, 1)
        logging.info("test_substract_egalite reussis")


    def test_substract_entier(self):
        "test des arguments"
        self.assertEqual(self.calculator.substract(6, "5"), "ERROR")
        self.assertNotEqual(self.calculator.substract("6", 5), 1)
        logging.info("test_substract_entier reussis")


class Testmultiply(unittest.TestCase):
    "Classe de test"

    logging.warning("Debut test methode multiply")

    def setUp(self):
        "init"
        self.calculator = module_calculator.SimpleCalculator()

    def test_multiply_egalite(self):
        "test egalite resultat"
        resultat = self.calculator.multiply(6, 5)
        self.assertEqual(resultat, 30)
        self.assertNotEqual(resultat, 31)
        resultat = self.calculator.multiply(-6, -5)
        self.assertEqual(resultat, 30)
        self.assertNotEqual(resultat, -30)
        self.assertEqual(self.calculator.multiply(-6, 5), -30)
        self.assertNotEqual(self.calculator.multiply(-6, 5), 30)
        self.assertEqual(self.calculator.multiply(6, -5), -30)
        self.assertNotEqual(self.calculator.multiply(6, -5), 30)
        logging.info("test_multiply_egalite reussis")



    def test_multiply_entier(self):
        "test des arguments"
        self.assertEqual(self.calculator.multiply(6, "5"), "ERROR")
        self.assertNotEqual(self.calculator.multiply("6", 5), 30)
        logging.info("test_multiply_entier reussis")


class Testdivision(unittest.TestCase):
    "Classe de test"

    logging.warning("Debut test methode division")

    def setUp(self):
        "init"
        self.calculator = module_calculator.SimpleCalculator()

    def test_division_egalite(self):
        "test egalite resultat"
        resultat = self.calculator.division(10, 5)
        self.assertEqual(resultat, 2)
        self.assertNotEqual(resultat, 3)
        resultat = self.calculator.division(-10, -5)
        self.assertEqual(resultat, 2)
        self.assertNotEqual(resultat, -2)
        self.assertEqual(self.calculator.division(-10, 5), -2)
        self.assertNotEqual(self.calculator.division(-10, 5), 2)
        self.assertEqual(self.calculator.division(10, -5), -2)
        self.assertNotEqual(self.calculator.division(10, -5), 2)
        logging.info("test_division_egalite reussis")


    def test_division_entier(self):
        "test des arguments"
        self.assertEqual(self.calculator.division(10, "5"), "ERROR")
        self.assertNotEqual(self.calculator.division(10, "5"), 2)
        self.assertEqual(self.calculator.division("10", 5), "ERROR")
        self.assertNotEqual(self.calculator.division("10", 5), 2)
        logging.info("test_division_entier reussis")


    def test_division_par_zero(self):
        "test division par zero"
        with self.assertRaises(ZeroDivisionError):
            self.calculator.division(10, 0)
        logging.info("test_division_par_zero reussis")




if __name__ == "__main__":
    unittest.main()
