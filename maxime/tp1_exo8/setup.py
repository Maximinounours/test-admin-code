from setuptools import setup

setup(
    name="PackageSimpleCalculator",
    version='0.0.1',
    author="Sophie et Maxime",
    package=['calculator'],
    description='package pour faire des calculs simples',
    license='GNU GPLv4',
    python_requires='>=2.4',
)