Gitlab commun sophie maxime

Les dossiers maxime et sophie contiennent à peu de choses près la même chose :
 - exo1.py exo2.py exo3.py
        Les fonctions demandées pour les exos 1, 2 et 3
- README.md 
        utilisé pour faire des tests de push, pull, fusion et les conflits de fusion
- calculator/
        dossier avec le package à créer jusqu'à l'étape de l'exo 7 inclus
- tp1/exo_8/
        dossier contenant le package disponible sur test.pypi
